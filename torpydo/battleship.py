import random
import os
import colorama
import platform
from colorama import Fore, Back, Style

from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController

myFleet = []
enemyFleet = []
shotsTakenByPC = []
rows = 8
lines = 8

def main():

    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def start_game():
    global myFleet, enemyFleet
    # clear the screen
    if(platform.system().lower()=="windows"):
        cmd='cls'
    else:
        cmd='clear'   
    os.system(cmd)
    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    while True:
        print("\n")
        print("*********************************************************************")
        print(Fore.YELLOW + r"Player, it's your turn" + Style.RESET_ALL)
        position = parse_position(input("Enter coordinates for your shot from given range A to H and 1 to 8. Press enter:"))
        if not is_position_valid(position):
            print(Fore.RED + r"Position not in field!" + Style.RESET_ALL)
            continue


        is_hit = GameController.check_is_hit(enemyFleet, position)
        if is_hit:
            print(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

        # print("Yeah ! Nice hit !" if is_hit else "Miss")
            print(Fore.GREEN + r"Yeah ! Nice hit !" + Style.RESET_ALL)
        else:
            print(Fore.RED + r"MISS" + Style.RESET_ALL)

        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        print()
        print(f"Computer shoot in {position.column.name}{position.row}")
        if is_hit:
            print(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

            print(Fore.RED + r"hit your ship!" + Style.RESET_ALL)

        else:
            print(Fore.GREEN + r"miss" + Style.RESET_ALL)

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    while position in shotsTakenByPC:
        letter = Letter(random.randint(1, lines))
        number = random.randint(1, rows)
        position = Position(letter, number)

    shotsTakenByPC.append(position)
    return position


def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print("Please position your fleet (Game board has size from A to H and 1 to 8) : ")

    myFleet[0].positions.append(Position(Letter.B, 4))
    myFleet[0].positions.append(Position(Letter.B, 5))
    myFleet[0].positions.append(Position(Letter.B, 6))
    myFleet[0].positions.append(Position(Letter.B, 7))
    myFleet[0].positions.append(Position(Letter.B, 8))

    myFleet[1].positions.append(Position(Letter.E, 6))
    myFleet[1].positions.append(Position(Letter.E, 7))
    myFleet[1].positions.append(Position(Letter.E, 8))
    myFleet[1].positions.append(Position(Letter.E, 9))

    myFleet[2].positions.append(Position(Letter.A, 3))
    myFleet[2].positions.append(Position(Letter.B, 3))
    myFleet[2].positions.append(Position(Letter.C, 3))

    myFleet[3].positions.append(Position(Letter.F, 8))
    myFleet[3].positions.append(Position(Letter.G, 8))
    myFleet[3].positions.append(Position(Letter.H, 8))

    myFleet[4].positions.append(Position(Letter.C, 5))
    myFleet[4].positions.append(Position(Letter.C, 6))
    # for ship in myFleet:
    #     print()
    #     print(f"Please enter the positions for the {ship.name} (size: {ship.size})")

    #     for i in range(ship.size):
    #         position_input = input(f"Enter position {i} of {ship.size} (i.e A3):")

    #         ship.add_position(position_input)

def initialize_enemyFleet():
    global enemyFleet

    enemyFleet = GameController.initialize_ships()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    enemyFleet[0].positions.append(Position(Letter.B, 6))
    enemyFleet[0].positions.append(Position(Letter.B, 7))
    enemyFleet[0].positions.append(Position(Letter.B, 8))

    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))
    enemyFleet[1].positions.append(Position(Letter.E, 9))

    enemyFleet[2].positions.append(Position(Letter.A, 3))
    enemyFleet[2].positions.append(Position(Letter.B, 3))
    enemyFleet[2].positions.append(Position(Letter.C, 3))

    enemyFleet[3].positions.append(Position(Letter.F, 8))
    enemyFleet[3].positions.append(Position(Letter.G, 8))
    enemyFleet[3].positions.append(Position(Letter.H, 8))

    enemyFleet[4].positions.append(Position(Letter.C, 5))
    enemyFleet[4].positions.append(Position(Letter.C, 6))

def is_position_valid(position):
    prow = position.row
    pcolumn = position.column.value

    if prow >= 1 and prow <= rows:
        if pcolumn >= 1 and pcolumn <= lines:
            return True

        else:
            return False
    else:
        return False

if __name__ == '__main__':
    main()
